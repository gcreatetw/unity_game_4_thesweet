﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CSLoadingState
{
    In,
    Out
}


public class CSCSLoading : MonoBehaviour {

    public CSLFProgressBar progressBar;

    public static CSLoadingState state;

    private void Start()
    {
        LeanTween.delayedCall(1f, Load);
    }

    private void OnDestroy()
    {
        LeanTween.cancel(gameObject);
    }

    protected void Load(string sceneName)
    {
        LeanTween.cancel(gameObject);
        LeanTween.value(gameObject, 0f, 1f, 0.9f).setOnUpdate((v) => {
            progressBar.value = v;
        });

        CSSceneManager.instance.LoadScene(sceneName, (percent, completed) => {
            //progressBar.value = percent;
        });
    }

    protected virtual void Load()
    {
        switch (state)
        {
            case CSLoadingState.In: Load("TheSweet"); break;
            case CSLoadingState.Out: Load("GameLobby"); break;
            default: break;
        }
    }

}
